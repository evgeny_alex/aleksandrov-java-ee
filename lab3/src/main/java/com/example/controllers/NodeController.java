package com.example.controllers;

import com.example.dto.NodeDto;
import com.example.mapper.NodeMapper;
import com.example.services.NodeService;
import com.example.services.OsmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@RestController("/node")
public class NodeController {
    
    @Autowired
    private NodeService nodeService;
    
    @Autowired
    private OsmService osmService;

    @GetMapping("/{id}")
    NodeDto getNode(@PathVariable("id") Long id) {
        return NodeMapper.toDTO(nodeService.read(id));
    }

    @PostMapping
    NodeDto createNode(@RequestBody NodeDto node) {
        return NodeMapper.toDTO(nodeService.create(NodeMapper.toDb(node)));
    }

    @PutMapping("/{id}")
    NodeDto updateNode(@PathVariable("id") Long id,
                       @RequestBody NodeDto node) {
        return NodeMapper.toDTO(nodeService.update(id, NodeMapper.toDb(node)));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        nodeService.delete(id);
    }

    @GetMapping("/search")
    public List<NodeDto> search(
            @RequestParam("latitude") Double latitude,
            @RequestParam("longitude") Double longitude,
            @RequestParam("radius") Double radius
    ) {
        return nodeService.search(latitude, longitude, radius).stream()
                .map(NodeMapper::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping("/init")
    public void init(
            @RequestParam("osmPath") String osmPath
    ) throws IOException, JAXBException, XMLStreamException {
        try (InputStream is = new FileInputStream(osmPath)) {
            osmService.parse(is);
        }
    }
}
