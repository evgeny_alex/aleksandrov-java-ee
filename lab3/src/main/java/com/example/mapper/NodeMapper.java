package com.example.mapper;

import com.example.dto.NodeDto;
import com.example.dto.generated.Node;
import com.example.entity.NodeEntity;
import com.example.entity.TagEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class NodeMapper {

    public static NodeEntity toDb(NodeDto NodeDto) {
        List<TagEntity> tagEntities = NodeDto.getTags().entrySet().stream()
                .map(t -> TagEntity.builder()
                        .key(t.getKey())
                        .value(t.getValue())
                        .build()).collect(Collectors.toList());
        return NodeEntity.builder()
                .latitude(NodeDto.getLatitude())
                .longitude(NodeDto.getLongitude())
                .name(NodeDto.getName())
                .tagEntities(tagEntities)
                .build();
    }

    public static NodeEntity toDb(Node node) {
        List<TagEntity> tagEntities = node.getTag().stream()
                .map(t -> TagEntity.builder()
                        .key(t.getK())
                        .value(t.getV())
                        .build()).collect(Collectors.toList());
        return NodeEntity.builder()
                .latitude(node.getLat())
                .longitude(node.getLon())
                .name(node.getUser())
                .tagEntities(tagEntities)
                .build();
    }

    public static NodeDto toDTO(NodeEntity nodeEntity) {
        Map<String, String> tags = nodeEntity.getTagEntities().stream().collect(Collectors.toMap(TagEntity::getKey, TagEntity::getValue));
        return new NodeDto(nodeEntity.getId(), nodeEntity.getName(), nodeEntity.getLongitude(), nodeEntity.getLatitude(), tags);
    }
}
