package com.example.services;

import com.example.entity.NodeEntity;
import com.example.repository.NodeRepository;
import com.example.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodeService {

    @Autowired
    private NodeRepository nodeRepository;
    
    @Autowired
    private TagRepository tagRepository;

    public NodeEntity create(NodeEntity nodeEntity) {
        tagRepository.saveAll(nodeEntity.getTagEntities());
        return nodeRepository.save(nodeEntity);
    }

    public NodeEntity read(Long id) {
        return nodeRepository.findById(id).orElseThrow(NullPointerException::new);
    }

    public NodeEntity update(Long id, NodeEntity nodeEntity) {
        NodeEntity nodeEntityFromDb = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        nodeEntity.setId(nodeEntityFromDb.getId());
        return nodeRepository.save(nodeEntity);
    }

    public void delete(long id) {
        NodeEntity nodeEntity = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        nodeRepository.delete(nodeEntity);
    }

    public List<NodeEntity> search(Double latitude, Double longitude, Double radius) {
        return nodeRepository.getNearNodes(latitude, longitude, radius);
    }
    
}
