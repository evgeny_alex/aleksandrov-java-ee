package com.example;

import com.example.model.StatisticModel;
import com.example.services.StatisticService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Map;

public class Application {

    private static final String STATISTIC_FORMAT = "%s : %d%n";
    public static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        try {
            if (args.length > 0) {
                StatisticService statisticService = new StatisticService(Files.newInputStream(Paths.get(args[0])));
                StatisticModel statistic = statisticService.getStatistics();
                System.out.println("Changes statistic:");
                printStatistic(statistic.getUsers());
                System.out.println("Tags statistic:");
                printStatistic(statistic.getTags());
            } else {
                logger.error("Enter path file.");
            }
        } catch (XMLStreamException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private static void printStatistic(Map<String, Integer> statistic) {
        statistic.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(e -> System.out.printf(STATISTIC_FORMAT, e.getKey(), e.getValue()));
    }
}

