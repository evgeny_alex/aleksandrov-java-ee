package com.example.services;

import com.example.Application;
import com.example.model.StatisticModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;

public class StatisticService {

    public static Logger logger = LoggerFactory.getLogger(Application.class);
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();
    private final XMLEventReader reader;

    private static final QName USER_ATTR_NAME = new QName("user");
    private static final QName ID_ATTR_NAME = new QName("id");
    private static final QName KEY_ATTR_NAME = new QName("k");
    private static final String NODE = "node";
    private static final String TAG = "tag";

    public StatisticService(InputStream is) throws XMLStreamException {
        reader = FACTORY.createXMLEventReader(is);
    }

    public StatisticModel getStatistics() throws XMLStreamException {

        StatisticModel result = new StatisticModel();

        try {
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (XMLStreamConstants.START_ELEMENT == event.getEventType()) {
                    StartElement startElement = event.asStartElement();
                    if (NODE.equals(startElement.getName().getLocalPart())) {
                        Attribute userAttribute = startElement.getAttributeByName(USER_ATTR_NAME);
                        result.userEditsCount(userAttribute.getValue());
                        Attribute idAttribute = startElement.getAttributeByName(ID_ATTR_NAME);
                        logger.debug("Processing tags for node with id {} start", idAttribute);
                        processTags(result, reader);
                        logger.debug("Tags processing finish");
                    }
                }
            }
        } catch (XMLStreamException e) {
            logger.error(e.getMessage());
        } finally {
            reader.close();
        }
        return result;
    }

    private static void processTags(StatisticModel result,
                                    XMLEventReader eventReader) throws XMLStreamException {
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (XMLStreamConstants.END_ELEMENT == event.getEventType() &&
                    NODE.equals(event.asEndElement().getName().getLocalPart())) {
                return;
            }
            if (XMLStreamConstants.START_ELEMENT == event.getEventType()) {
                StartElement startElement = event.asStartElement();
                if (TAG.equals(startElement.getName().getLocalPart())) {
                    Attribute key = startElement.getAttributeByName(KEY_ATTR_NAME);
                    result.incrementTagCount(key.getValue());
                }
            }
        }
        throw new XMLStreamException("Unexpected end of stream");
    }

}
