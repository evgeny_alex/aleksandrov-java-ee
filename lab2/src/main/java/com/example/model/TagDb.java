package com.example.model;

import com.example.model.generated.Node;
import lombok.Data;

@Data
public class TagDb {
    private final long nodeId;
    private final String key;
    private final String value;

    public static TagDb convert(Node.Tag tag, long nodeId) {
        return new TagDb(nodeId, tag.getK(), tag.getV());
    }
}
