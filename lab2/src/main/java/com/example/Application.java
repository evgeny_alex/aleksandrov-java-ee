package com.example;

import com.example.dao.NodeDao;
import com.example.dao.NodeDaoImpl;
import com.example.dao.TagDao;
import com.example.dao.TagDaoImpl;
import com.example.services.NodeService;
import com.example.services.OsmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public class Application {

    public static Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws SQLException {
        if (args.length < 1) {
            throw new IllegalArgumentException("Wrong argument count");
        }
        LOG.info("File decompressing start");
        try (InputStream inputStream = Files.newInputStream(Paths.get(args[0]))) {
            LOG.info("File decompressing finish");
            DbUtils.init();
            NodeDao nodeDao = new NodeDaoImpl();
            TagDao tagDao = new TagDaoImpl();
            NodeService nodeService = new NodeService(nodeDao, tagDao);
            OsmService osmService = new OsmService(nodeService);
            osmService.process(inputStream);
        } catch (FileNotFoundException e) {
            LOG.error("File not found", e);
        } catch (IOException e) {
            LOG.error("File read error", e);
        } catch (JAXBException | XMLStreamException e) {
            LOG.error("File processing error", e);
        } catch (SQLException e) {
            LOG.error("Failed to initialize database", e);
        } finally {
            DbUtils.closeConnection();
        }
    }
}