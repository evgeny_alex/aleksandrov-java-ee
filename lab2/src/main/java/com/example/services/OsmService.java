package com.example.services;

import com.example.model.NodeDb;
import com.example.model.generated.Node;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

@AllArgsConstructor
public class OsmService {
    private static final Logger LOG = LoggerFactory.getLogger(OsmService.class);
    private static final String NODE = "node";

    private final NodeService nodeService;

    public void process(InputStream inputStream) throws JAXBException, XMLStreamException {
        LOG.info("OSM processing start");
        long start = System.currentTimeMillis();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        try {
            reader = factory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int event = reader.next();
                if (XMLStreamConstants.START_ELEMENT == event && NODE.equals(reader.getLocalName())) {
                    processNode(jaxbContext, reader);
                }
            }
        } finally {
            assert reader != null;
            reader.close();
        }
        nodeService.flush();
        long end = System.currentTimeMillis();
        LOG.info("Elapsed time, ms: " + (end - start));
        LOG.info("OSM processing finish");
    }

    private void processNode(JAXBContext jaxbContext, XMLStreamReader reader) throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Node node = (Node) unmarshaller.unmarshal(reader);
        nodeService.putNodeWithPreparedStatement(NodeDb.convert(node));
    }

}
